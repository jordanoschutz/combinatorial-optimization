using JuMP
using GLPK

# Tarefas
p = [2 10 4]
# Tamanho da entrada
n = length(p)
# Constante com os valores mínimos entre cada par de tarefas
k = [0 2 2 ; 2 0 4 ; 2 4 0]

# Big-M
M = sum(p)

m = Model(with_optimizer(GLPK.Optimizer))

@variable(m, s[1:n] >= 0)
@variable(m, t >= 0)
@variable(m, x[1:n, 1:n], Bin) # 1 se tarefa i executa antes da tarefa j; 0, c.c.

@constraint(m, [i = 1:n], t >= s[i] + p[i]) # Makespan
@constraint(m, [i = 1:n, j = 1:n, i != j], (s[j] - s[i]) + M * x[i, j] >= k[i, j])
@constraint(m, [i = 1:n, j = 1:n, i != j], (s[i] - s[j]) + M * (1 - x[i, j]) >= k[i, j])

@objective(m, Min, t)

println(m)

optimize!(m)

println("A solução ótima é: $(objective_value(m))")
println("com:")

for i in 1:n
    println("s$i = $(value(s[i]))")
end
