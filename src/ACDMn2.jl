using JuMP
using GLPK

# Tarefas
p = [2 10]
# Tamanho da entrada
n = length(p)
# Constante com os valores mínimos entre cada par de tarefas
k = 2 # min(p)
# Big-M
M = sum(p)
m

m = Model(with_optimizer(GLPK.Optimizer))

@variable(m, s[1:n] >= 0)
@variable(m, t >= 0)
@variable(m, x, Bin) # 1 se tarefa 1 executa antes da tarefa 2

@constraint(m, t >= s[1] + p[1])
@constraint(m, t >= s[2] + p[2])
@constraint(m, (s[2] - s[1]) + M * x >= k)       # Tarefa 1 executa antes da tarefa 2
@constraint(m, (s[1] - s[2]) + M * (1 - x) >= k) # Tarefa 2 executa antes da tarefa 1

@objective(m, Min, t)

println(m)

optimize!(m)

println("A solução ótima é: $(objective_value(m))")
println("com:")

for i in 1:n
    println("s$i = $(value(s[i]))")
end
