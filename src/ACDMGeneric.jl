using JuMP
using GLPK

tasks = Vector{Int}()

open("tests/trsp_50_1.dat") do file
    alllines = readlines(file)

    for i in 2:length(alllines)
        push!(tasks, parse(Int, alllines[i]))
    end
end

n = length(tasks)
k = zeros(Int, n, n)

for i in 1:n
    for j in 1:n
        if i == j
            k[i, j] = 0
        else
            k[i, j] = min(tasks[i], tasks[j])
        end
    end
end

M = sum(tasks)

model = Model(with_optimizer(GLPK.Optimizer))

@variable(model, s[1:n] >= 0)
@variable(model, t >= 0)
@variable(model, x[1:n, 1:n], Bin) # 1 se tarefa i executa antes da tarefa j; 0, c.c.

@constraint(model, [i = 1:n], t >= s[i] + tasks[i]) # Makespan
@constraint(model, [i = 1:n, j = 1:n, i != j], (s[j] - s[i]) + M * x[i, j] >= k[i, j])
@constraint(model, [i = 1:n, j = 1:n, i != j], (s[i] - s[j]) + M * (1 - x[i, j]) >= k[i, j])

@objective(model, Min, t)

println(model)

optimize!(model)

println("A solução ótima é: $(objective_value(model))")
println("com:")

for i in 1:n
    println("s$i = $(value(s[i]))")
end
